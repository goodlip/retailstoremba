USE [master]
GO
/****** Object:  Database [BugibbaTransactions]    Script Date: 16/11/2019 11:56:22 ******/
CREATE DATABASE [BugibbaTransactions]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BugibbaTransactions', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BugibbaTransactions.mdf' , SIZE = 139264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BugibbaTransactions_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\BugibbaTransactions_log.ldf' , SIZE = 204800KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [BugibbaTransactions] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BugibbaTransactions].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BugibbaTransactions] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET ARITHABORT OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BugibbaTransactions] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BugibbaTransactions] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BugibbaTransactions] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BugibbaTransactions] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET RECOVERY FULL 
GO
ALTER DATABASE [BugibbaTransactions] SET  MULTI_USER 
GO
ALTER DATABASE [BugibbaTransactions] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BugibbaTransactions] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BugibbaTransactions] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BugibbaTransactions] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BugibbaTransactions] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'BugibbaTransactions', N'ON'
GO
ALTER DATABASE [BugibbaTransactions] SET QUERY_STORE = OFF
GO
USE [BugibbaTransactions]
GO
/****** Object:  Table [dbo].[transactionheaders]    Script Date: 16/11/2019 11:56:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transactionheaders](
	[company_name] [nvarchar](256) NULL,
	[number] [int] NOT NULL,
	[date] [datetime] NULL,
	[time] [time](7) NULL,
	[type] [nvarchar](50) NULL,
	[total] [numeric](6, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [header_un] UNIQUE NONCLUSTERED 
(
	[number] ASC,
	[date] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[transactionlines]    Script Date: 16/11/2019 11:56:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[transactionlines](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[transaction_number] [int] NOT NULL,
	[item_number] [nvarchar](256) NULL,
	[description] [nvarchar](50) NULL,
	[quantity] [int] NULL,
	[unit_price] [numeric](5, 2) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [BugibbaTransactions] SET  READ_WRITE 
GO
