--creating tables for transaction headers and lines
create table dbo.transactionheaders(
	company_name nvarchar(256),
	number	int primary key,
	[date]	datetime,
	[time]	time,
	[type]	nvarchar(50),
	total	numeric(6,2),
	constraint header_un unique(number, [date])
)

create table dbo.transactionlines(
	id int primary key identity(1,1),
	transaction_number int not null,
	[description] nvarchar(256),
	item_number nvarchar(50),
	quantity int,
	unit_price numeric(5,2)
)

drop table transactionlines
drop table transactionheaders

delete from transactionlines
delete from transactionheaders

--exploring data
select * from transactionheaders where number = 440240
select * from transactionlines where transaction_number = 440240

select count(*) from transactionheaders
select count(*) from transactionlines

select * from transactionheaders
select * from transactionlines

--reming columns
EXEC sp_RENAME 'transactionlines.item number' , 'item_number', 'COLUMN'

select distinct * from transactionlines where item_number = 'JA_012' 

use FguraTransactions

select * from transactionheaders
select * from transactionlines 

delete from transactionheaders
delete from transactionlines

--creating table to store transactions formatted for data mining
create table CombinedTransactions(
	transactionId numeric primary key identity(1,1),
	transactionNumber numeric unique not null,
	transactionItems nvarchar(MAX) not null
)

--cleaning more where there are no lines associated with 
--a transaction header
DECLARE @MyCursor CURSOR;
DECLARE @MyField int;
BEGIN
    SET @MyCursor = CURSOR FOR
    select  number from transactionheaders      

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @MyField

    WHILE @@FETCH_STATUS = 0
    BEGIN
      if(0 = (select count(tl.item_number) 
		from transactionlines tl join transactionheaders th
		on tl.transaction_number = th.number
		where th.number = @MyField))
			delete from transactionheaders
			where transactionheaders.number = @MyField

      FETCH NEXT FROM @MyCursor 
      INTO @MyField 
    END; 

    CLOSE @MyCursor ;
    DEALLOCATE @MyCursor;
END;

--cleaning data where there is only one item in a transaction
declare @Cursor cursor;
declare @Field int;
BEGIN
    SET @Cursor = cursor for
    select number from transactionheaders      

    open @Cursor 
    fetch next from @Cursor 
    into @Field

    while @@FETCH_STATUS = 0
    begin
		if(1 = (select count(item_number) 
			from transactionlines
			where transaction_number = @Field))
		begin
			delete from transactionheaders
			where number = @Field

			delete from transactionlines
			where transaction_number = @Field
		end
      fetch next from @Cursor 
      into @Field 
    end; 

    close @Cursor ;
    deallocate @Cursor;
end;


--inserting data formatted for data mining(using item number)

insert into CombinedTransactions
	(transactionNumber, transactionItems)
SELECT th.number, 
concat('[',left(+SUBSTRING(
    (
		SELECT distinct tl.item_number+','
        FROM dbo.transactionlines tl
        WHERE tl.transaction_number = th.number
        FOR XML PATH ('')
    ), 1, 10000) , len(SUBSTRING(
    (
		SELECT distinct tl.item_number+','
        FROM dbo.transactionlines tl
        WHERE tl.transaction_number = th.number
        FOR XML PATH ('')
    ), 1, 10000))-1),']')
FROM transactionheaders th

--creating tables to store categorised and pre-categorisation transactions
create table categorised_transactions(
	id int primary key identity(1,1),
	transaction_number int not null,
	[description] nvarchar(256),
	item_number nvarchar(50),
	quantity int,
	unit_price numeric(5,2)
)

create table pre_process_transactions(
	id int primary key identity(1,1),
	transaction_number int not null,
	[description] nvarchar(256),
	item_number nvarchar(50),
	quantity int,
	unit_price numeric(5,2)
)

--testing out if more association rules are extracted from the data if
-- more categories are present.
create table pre_process_transactions_test(
	id int primary key identity(1,1),
	transaction_number int not null,
	[description] nvarchar(256),
	item_number nvarchar(50),
	quantity int,
	unit_price numeric(5,2)
)

insert into pre_process_transactions
	(transaction_number, [description], item_number, quantity, unit_price)
select	transaction_number, 
		[description], 
		item_number, 
		quantity, 
		unit_price
from transactionlines


insert into pre_process_transactions_test
	(transaction_number, [description], item_number, quantity, unit_price)
select	transaction_number, 
		[description], 
		item_number, 
		quantity, 
		unit_price
from transactionlines


--transactions formatted for data mining(using item descriptions)
create table CombinedTransactionDescriptions(
	transactionId numeric primary key identity(1,1),
	transactionNumber numeric unique not null,
	transactionItems nvarchar(MAX) not null
)

--Categorisation of items
declare @id	int,
		@description nvarchar(max)

declare line_cursor cursor
for select [description], id from pre_process_transactions

open line_cursor

fetch next from line_cursor into @description, @id
while @@FETCH_STATUS = 0 
	begin
		if(@description like 'SLICE%' or @description like '%cake%' or @description like '%doughnut%' or @description like '%profiteroles%' 
			or @description like '%milka%' or @description like '%perugina%' or @description like '%kit kat%' or @description like '%kinder%' 
			or @description like '%dairy%milk' or @description like '%cote dor%' or @description like '%bahlsen%' or @description like '%chocolate%'
			or @description like '%vanilla%' or @description like '%biscuits%' or @description like '%choc%' or @description like '%gateaux%'
			or @description like '%malteser%' or @description like '%caramel%' or @description like '%cookies%' or @description like '%nutella%'
			or @description like '%maynards%' or @description like '%CHRISTMAS LOG%' or @description like '%ICE CREAM%' or @description like '%magnum%'
			or @description like '%CANNOLI%' or @description like '%granita%' or @description like '%loacker%' or @description like '%mulino%bianco%'
			or @description like '%castella%' or @description like '%waffles%' or @description like '%M&M%' or @description like '%skittles%'
			or @description like '%easter%' or @description like '%perlini%' or @description like '%cornetto%' or @description like '%novi%'
			or @description like '%APPLE PUFF%' or @description like '%APPLE PIE%' or @description like '%trifle%' or @description like '%ice-cream%'
			or @description like '%cassata%' or @description like '%APPLE STRUDEL%' or @description like '%tronky%' or @description like '%calippo%'
			or @description like '%mars%' or @description like '%gelat%' or @description like '%rum baba%'or @description like '%aero%' 
			or @description like '%after eight%'or @description like '%tart%' or @description like '%aunt%' or @description like '%cookie%'
			or @description like '%baci%' or @description like '%bau%' or @description like '%bcrocker%' or @description like '%bolands%'or @description like '%bounty%'
			or @description like '%bourbon%' or @description like '%bt%' or @description like '%cabrioni%' or @description like '%cad%' or @description like '%custard%'
			or @description like '%expo%' or @description like '%ferrero%' or @description like '%figolla%' or @description like '%sicilian%' or @description like '%mille%foglie%'
			or @description like '%sinizza%' or @description like '%snickers%' or @description like '%fruit flan%' or @description like '%starburst%' or @description like '%strawberry%'
			or @description like '%krustini%' or @description like '%lolly%' or @description like '%cornet%' or @description like '%swizzles%' or @description like '%sweet%'
			or @description like '%wafer%' or @description like '%tiramisu%' or @description like '%gummi%' or @description like '%toblerone%' or @description like '%torta%'
			or @description like '%truffles%' or @description like '%twix%' or @description like '%pasqua%' or @description like '%werthers%' or @description like '%biskuttelli%'
			or @description like '%black forest%' or @description like '%chupa%' or @description like '%lindt%' or @description like '%macaroons%' or @description like '%devon%' 
			or @description like '%digestive%' or @description like '%disp%' or @description like '%eclairs%' or @description like '%duplo%' or @description like '%excellence%' 
			or @description like '%rolls%' or @description like '%ga slice%' or @description like '%ga yog%' or @description like '%galaxy%' or @description like '%gecchele%' or @description like '%halls%' 
			or @description like '%panna spray%' or @description like '%jelly%' or @description like '%helwa%' or @description like '%kitkat%' or @description like '%lindor%' 
			or @description like '%kwarezimal%' or @description like '%7%DY%' or @description like '%AGDC%' or @description like '%ALMA%' or @description like '%bb%' or @description like '%buskuttini%'
			or @description like '%carte d%' or @description like '%cream crackers%' or @description like '%muffins%' or @description like '%fruit roll%' or @description like '%godiva%'
			or @description like '%gullon%' or @description like 'hill%' or @description like 'hmp%' or @description like 'hochland%' or @description like 'honey fingers%' or @description like 'ice lollies%'
			or @description like 'jam puff%' or @description like 'jazzles%' or @description like 'calippo%' or @description like 'lion %' or @description like 'maoam %' or @description like 'mentos%'
			or @description like 'monte %' or @description like 'mul mixare%' or @description like 'nero tavoletta%' or @description like 'nic%' or @description like 'orbit%' or @description like 'oreo%' 
			or @description like 'profiterole%' or @description like 'qaghaq %' or @description like 'quanlity street%' or @description like 'retina %' or @description like 'ringo%' or @description like '%rolo%'
			or @description like 'smarties%' or @description like 'sorbet %' or @description like 'sorini %' or @description like '%muffin%' or @description like 'tic tac%' or @description like 'tl %'
			or @description like 'zeppoli%' or @description like 'zero %' or @description like '%crumble%' or @description like '%maple%' or @description like 'astra%' or @description like 'balocco%'
			or @description like 'belfine %' or @description like 'biskuttini %' or @description like 'brown sugar%' or @description like '%dolly%' or @description like '%gum%' or @description like 'coconut %'
			or @description like 'conosnack %' or @description like 'crostatine %' or @description like '% creams %' or @description like 'frosta %' or @description like 'gourmand %' or @description like 'icam %'
			or @description like 'kalippo %' or @description like 'kras %' or @description like 'lol %' or @description like 'looney %' or @description like 'lovemore %' or @description like '%prince%'
			or @description like 'maxus %' or @description like 'mcv %' or @description like 'melbourne %' or @description like 'merba %' or @description like '% banoffee %' or @description like 'mul %' 
			or @description like 'munchies %' or @description like 'nestle %' or @description like 'oscar %' or @description like 'ottijiet%' or @description like 'pan %' or @description like 'pannacotta %'
			or @description like 'pastini %' or @description like 'pectol %' or @description like 'paw patrol %' or @description like 'perfection %' or @description like 'prinjolata %'
			or @description like 'puff pastry %' or @description like '%QAGHAQ%' or @description like '%quality street%' or @description like '%rashers%' or @description like '%reigel%' or @description like 'pumpkin %'
			or @description like 'antonelli %' or @description like 'birds %' or @description like 'wensley%' or @description like 'vz %' or @description like 'tutti frutti%' or @description like 'tunnocks %' 
			or @description like 'trefin %' or @description = 'weekly fresh pastry special'  or @description like 'stroopwafel %' or @description like 'stella %' or @description like '% nougat %' or @description like 'short crust pastry %'
			or @description like 'sfera%' or @description like 'rowntrees %' or @description like 'retina2d %' or @description like 'raspberry cranberry %' or @description like 'quintin %' or @description like 'polo %' or @description like 'panna cotta %'
			or @description like '%banoffee%' or @description like 'midi %' or @description like '7 days %' or @description like '%dark hazelnut%' or @description like 'cucciolone %' or @description = 'fingers kbar' or @description like 'fiordilatte%'
			or @description like 'wall%twister%' or @description like 'rowntree%' or @description like 'retina2%' or @description like 'pz al latte%' or @description like 'pg nero%' or @description like 'mr. jim%' or @description like '%biskutelli%'
			or @description like '%knupsing%' or @description like '%panettone%' or @description like 'candy pop%' or @description like 'christmas pudding%' or @description like 'bergen %' or @description like 'dlle %' or @description = 'dolci mini'
			or @description like 'fingers %' or @description like 'grisbi%' or @description like 'huntley%' or @description like 'icing %' or @description like 'marzipan %' or @description like 'may bas %' or @description like 'options %'
			or @description like 'pudding %' or @description like 'rocher %' or @description like '%profiterol%' or @description like 'flaky cream %' or @description like 'foxy %' or @description like 'fully coated %' or @description = 'marzipan'
			or @description like '%ciokopanna%' or @description like 'sacher %' or @description like 'sea figures %' or @description like 'vecchio %' or @description like 'yoomoo %' or @description like 'nero uovo %' or @description like 'celebrations %'
			or @description like 'lemon meringue %' or @description like 'marble %')
			begin
				update pre_process_transactions
				set description = 'CAKES AND SWEETS'
				where id = @id
			end
		
		if(@description like '%nuts%' or @description like '%lorenz%' or @description like '%cashews%' or @description like '%pisacch%' or @description like '%almonds%' or @description like '%fp%'
			or @description like '%trail mix%') 
			begin
				update pre_process_transactions
				set description = 'NUTS'
				where id = @id
			end

		if(@description like 'MILK %' or @description like 'SKIMMED MILK %' or @description like '%YOGURT%' or @description like '%WHOLE MILK%' or @description like '%ZOTT%'
			or @description like '%EVAPORATED%' or @description like '%GBEJNIET%' or @description like '%MEADOW%' or @description like '%milk%' or @description like '%gbejna%'
			or @description like '%skimmed%' or @description like '%besciamel%' or @description like '%ricotta%' or @description like '%kolios%' or @description like '%delta%' 
			or @description like '%soia%' or @description like '%f.flag%' or @description like '%gra%' or @description like '%irkotta%' or @description like '%hanini%' 
			or @description like '%hopla%' or @description like '%exquisa%' or @description like '%fresh cream%' or @description like 'hellema%' or @description like 'ktf%' or @description like 'kth%' 
			or @description like 'mevgal%' or @description like 'mozzarella%' or @description like 'oly%' or @description like 'orange creams%' or @description like 'vqr %' or @description like 'arla %'
			or @description like 'boccone%' or @description like 'carnation %'or @description like ' CI%' or @description like 'ideal %' or @description like '% babybel %' or @description like 'pecorino %'
			or @description like 'philadelphia %' or @description like 'white sauce%' or @description like 'ricotta %' or @description like 'rem sunf %' or @description like '%yoghurt%' or @description like 'CI%' 
			or @description like 'frico %' or @description like 'vive soy natural%' or @description like 'trev latte%' or @description like 'rikotta %' or @description like 'rem %' or @description like 'tgl elma %' 
			or @description like '%whipping cream%' or @description like 'castello%' or @description like 'cathedral %' or @description like '%formaggi%' or @description like 'happy cow%' or @description like 'jocca%'
			or @description like 'president %' or @description like 'provamel %' or @description like 'regilait %' or @description like '%latte%' or @description like 'soresina %' or @description like '%butter%'
			or @description like '%MOZZARELLA%' and @description not like '%PIZZA%MOZZARELLA%' or @description like '%CHEESE%' or @description like '%LEERDAMMER%' or @description like '%GOBON%' or @description like '%EDAMER%' or @description like '%emment%'
			or @description like '%danish%' or @description like '%cheddar%' or @description like '%ambr%' or @description like '%baronesse%' or @description like '%edam%' or @description like '%camembert%'
			or @description like '%fdv%' or @description like '%scamorza%' or @description like '%parmig%' or  @description like '%solo%italia%' or @description like '%grana%'
			or @description like '%parmeg%' or @description like '%formaggio%' or @description like '%zanetti%' or @description like '%gpadano%')
			begin
				update pre_process_transactions
				set description = 'MILK AND DAIRY PRODUCTS'
				where id = @id
			end

		if(@description like '%dip%' or @description like '%bigilla%' or @description like '%josefa%')
			begin
				update pre_process_transactions
				set description = 'DIPS'
				where id = @id
			end

		if(@description like '%barilla%' or @description like '%tortellini%' or @description like '%ravioli%' or @description like '%rigatoni%'
		or @description like '%rummo%' or @description like '%pasta%' or @description like '%spaghetti%' or @description like '%carbonara%' or @description like '%tagliatelle%'
		or @description like '%gnocchi%' or @description like '%molisana%' or @description like '%penne%' or @description like 'poiatti%' or @description like '%fusilli%' or @description like '% farfalle %'
		or @description like 'twistd med %' or @description like '%spagetti%')
			begin 
				update pre_process_transactions
				set description = 'PASTA'
				where id = @id
			end

		if(@description like '%cereal%' or @description like '%kelloggs%' or @description like '%kellogs%' or @description like '%weetabix%')
			begin
				update pre_process_transactions
				set description = 'CEREAL AND CEREAL BARS'
				where id = @id
			end

		if(@description like '%chip%' or @description like '%pizza%' or @description like '%frozen%' or @description like '%alberto%'
		 or @description like '%findus%' or @description like '%aia%' or @description like '%american%' or @description like '%buitoni%'
		 or @description like '%dijj%' or @description like '%eurofreez%' or @description like '%wings%' or @description like '%cordon%'
		 or @description like '%derby%' or @description like '%duj% ' or @description like '%emborg%' or @description like '%golden foods%'  or @description like '%greens%'
		 or @description like '%gv%' or @description like '%arbi%' or @description like '%arancini%' or @description like '%duj%' or @description like '%spring roll%' or @description like '%meatballs%'
		 or @description like '%fries%' or @description like '%golden%' or @description like 'kul%' or @description like 'kut%' or @description like 'mc cain%' or @description like 'mil kcina%'
		 or @description like '%smileys%' or @description like 'wedges %'or @description like 'gesnoin %' or @description like 'mccain %' or @description like '% kievs %' 
		 or @description like '%mash potatoes%' or @description like 'pizzini %' or @description like 'polpetti %' or @description like 'wellino%' or @description like 'vol%' or @description like 'sharwoods%'
		 or @description like 'sadia%' or @description like '%patate fritte%' or @description like 'pizzini %' or @description like '1OFF %' or @description like '%vegetable quarter pounders%' or @description like 'anchovyfillets %'
		 or @description like 'angels on %' or @description like 'coc spicy %' or @description like 'dospo %' or @description like '%party rings%' or @description like 'savoury%' or @description like 'polipetti%' or @description like 'pizzini%'
		 or @description like 'omni crunch%' or @description like '%chickpopc%' or @description like '%straight cut%' or @description like 'gour irish %' or @description like 'country%' or @description like 'baked potatoes%' or @description like 'feletti%'
		 or @description like 'g.i.%' or @description like 'orogel %' or @description = 'potato pillows' or @description like 'quorn %' or @description like '%wedges%' or @description like 'cranberries%' or @description like 'pangasius %'
		 or @description like '%samosa%' or @description like '% pangasius %' or @description like 'vegetable fingers %' or @description like 'maggi %' or @description like 'merci %' or @description like 'filetti di alici %')
			begin
				update pre_process_transactions
				set description = 'FROZEN ITEMS'
				where id = @id
			end

		if(@description like '%lactose%free%' or @description like '%alpro%')
			begin
				update pre_process_transactions
				set description = 'LACTOSE FREE ITEMS'
				where id = @id
			end

		if(@description like '%sugar%free%' or @description like '%senza zucchero%' or @description like 'zeros%')
			begin
				update pre_process_transactions
				set description = 'SUGAR FREE ITEMS'
				where id = @id
			end

		if(@description like '%glu%free%' or @description like '%goodfellas%' or @description like '%g/free%')
			begin
				update pre_process_transactions
				set description = 'GLUTEN FREE ITEMS'
				where id = @id
			end

		if(@description like '%fish%' or @description like '%salmon%' or @description like '%cod%' or @description like '%prawn%' or @description like 'alaska%'
			or @description like 'gamberetti %' or @description like 'gamberoni %' or @description like 'klaas %' or @description like 'mackerel %' or @description like '% shrimps %'
			or @description like '%qarnita%' or @description like '%tuna pouch%' or @description like '%octopus%' or @description like '%tuna%' or @description like 'chilean whole %')
			begin
				update pre_process_transactions
				set description = 'FISH PRODUCTS'
				where id = @id
			end

		if(@description like '%almonds%' or @description like '%nuts%' or @description like '%walnut%' or @description like '%almond%' or @description like '%assorted%' or @description like 'cashew%'
		or @description like 'craisins %' or @description like 'pistachios %')
			begin
				update pre_process_transactions
				set description = 'NUTS'
				where id = @id
			end

		if(@description like '%coke%' or @description like '%sprite%' or @description like '%ice tea%' or @description like '%nestea%' 
			or @description like '%i/tea%'or @description like '%safari%' or @description like '%kinnie%' or @description like '%bravo%' or @description like '%7 up%'
			or @description like '%belte%'or @description like '%red%bull%' or @description like '%pepsi%' or @description like '%dr pepper%' or @description like '%rauch%'
			or @description like '%pfanner%' or @description like '%cola%' or @description like '%mirinda%' or @description like '%juice%' or @description like '%schweppes%'
			or @description like '%tropicana%' or @description like '%the''%' or @description like '%fizzy%' or @description like '%britvic%' or @description like '%f l%' 
			or @description like '%f o%' or @description like '%gatorade%' or @description like 'inno%' or @description like 'orange drink%' or @description like 'ribena%' or @description like 'bifrutas%'
			or @description like 'enervit %' or @description like 'frutana %' or @description like 'GO & %' or @description = 'peach drink' or @description like '%energy drink%' or @description like 'vital %'
			or @description like 'veroni %' or @description like 'cp/valencia %' or @description like 'echo %' or @description like 'd dutch%' or @description like 'nocco %' or @description like 'powerade %'
			or @description like '% nectar %')
			begin
				update pre_process_transactions
				set description = 'SUGARY DRINKS'
				where id = @id
			end

		if(@description like 'MALTESE WHITE%' or @description like 'MALTESE BROWN%' or @description like 'BAKED% BAGUETTE%' or 
			@description like 'CIABATTA%' or @description like 'FTIRA %' or @description like 'HOT DOG BUNS%' or @description like 'SLICED WHITE LOAF%' 
			or @description like 'PANINI TAL-KWART%' or @description like 'RESTAURANT BURGERS%' or @description like 'RESTAURANT FTIRA SMALL%' or
			@description like 'KAISER ROLLS%' or @description like '%BURGER BUNS%' or @description like '%XIKLUN%' or @description like '%BUN%' or @description like '%LOW GI%'
			or @description like '%SUNFLOWER ROLL%' or @description like '%LOAF%' or @description like '%bread%' or @description like '%PITTA%' or @description like 'la fiesta%'
			or @description like 'mission%' or @description like 'poco loco%' or @description like 'restaurant %' or @description like 'argal%' or @description like '%wraps%' 
			or @description like 'classic white %' or @description like 'completa %' or @description like 'dijo %' or @description like 'tortilla %' or @description like '%ciabatta%'
			or @description like 'old elpaso %' or @description like 'plain %' or @description like 'baked stirata %' or @description like 'vitavigor%' or @description like '% baguette %'
			or @description like '% tortilla % ' or @description like '%baguette%' or @description like '%wrap%' or @description like '%tortilla%' or @description like 'genius%'
			or @description like 'maltese%pinwheels%' or @description like '%frollino%' or @description = 'BREAD AND WRAPS')
			begin
				update pre_process_transactions
				set description = 'BREAD'
				where id = @id
			end

		if(@description like '%coffee%' or @description like '%starbucks%' or @description like '%drink%choc%' or @description like '%tea%' or @description like '%nescafe%'
		or @description like '%capuccino%' or @description like '%chocochino%' or @description like '%lavazza%' or @description like '%saquella%' or @description like '%twinnings%'
		or @description like '%ciobar%' or @description like 'ndg%' or @description like 'camomile%' or @description like '% cafe %' or @description like 'twinning%'  or @description like 'tetley %'
		or @description like 'sonata%' or @description like 'twinings%' or @description like 'maxwell%' or @description = 'hot drink' or @description like '%coffe%' or @description like 'schneekoppe %')
			begin
				update pre_process_transactions
				set description = 'HOT DRINKS'
				where id = @id
			end

		if(@description like '%TIMPANA%' or @description like '%QASSATA%' or @description like '%pastizzi%' or @description like '%tea%' or @description like '%pie%' or @description like 'sandwich %')
			begin
				update pre_process_transactions
				set description = 'HOT FOOD'
				where id = @id
			end

		if(@description like '%RICE%' or @description like '%riso%' or @description like '%tilda%')
			begin
				update pre_process_transactions
				set description = 'RICE'
				where id = @id
			end

		if(@description like '%BAKE%ROLLS%' or @description like '%blisters%' or @description like '%crix%' or @description like '%crisps%' or @description like '%doritos%'
		or @description like '%el sabor%' or @description like '%ritz%' or @description like '%space balls%' or @description like '%spiros%' or @description like '%walkers%'
		or @description like '%snacks%' or @description like '%festa%' or @description like '%gobblers%' or @description like '%bake rolls%' or @description like '%crich%'
		or @description like '%crips%' or @description like 'jumbo%' or @description like 'lu tuc%' or @description like 'oscar grissini%' or @description like 'pringles%'
		or @description like 'real crips%' or @description like 'tal%furnar%' or @description like 'tastees%' or @description like 'twistees%' or @description like 'boni %'
		or @description like 'catch %' or @description like 'dlf %' or @description like '% rings %' or @description like 'jungle sticks %' or @description like 'krips %' 
		or @description like 'krops %' or @description like 'maltese platter %' or @description like 'pata %' or @description like 'pavesi %' or @description like 'pom poms %'
		or @description like 'skips%' or @description like '2pktscr%' or @description = 'agros sesame snack' or @description like 'cocktail crackers %' or @description like 'fiori d%'
		or @description like 'schnkoppe%' or @description like 'schar%' or @description like 'party time%' or @description = 'krips' or @description like '2 pktscrmcrackers%'
		or @description like 'ganchitos %' or @description = 'cheesy rings' or @description like 'dr schar %' or @description like 'gb gf%' or @description like 'pretzels %'
		or @description like 'stikado %' )
			begin
				update pre_process_transactions
				set description = 'SNACKS'
				where id = @id
			end

		if(@description like '%OIL%' or @description like 'carapelli%')
			begin
				update pre_process_transactions
				set description = 'OIL'
				where id = @id
			end

		if(@description like '%SOUP%'or @description like '%NOODLE%' or @description like '%gusparo%' or @description like '%knorr%' or @description like '%stockpot%' or @description like '%minestra%'
		or @description like '%fried nood%' or @description like 'kawlata %' or @description like 'ind mie %' or @description like 'ind special %')
			begin
				update pre_process_transactions
				set description = 'SOUPS AND NOODLES'
				where id = @id
			end

		if(@description like '%BEEF%' or @description like '%CHICKEN%' or @description like '%PORK%' or @description like '%WURSTEL%'
		or @description like '%BACON%' or @description like '%FIORUCCI%' or @description like '%wudy%' or @description like '%sausage%'
		or @description like '%salami%' or @description like '%veal%' or @description like '%3-star%' or @description like '%amadori%'
		or @description like '%big als%' or @description like '%burger%' or @description like '%butchersbarn%' or @description like '%chorizo%'
		or @description like '%ham%' or @description like '%gammon%' or @description like '%turkey%' or @description like '%arrosto%' or @description like '%streaky%'
		or @description like '%collar%' or @description like '%mortadella%' or @description like '%cube roll%' or @description like '%dog%' or @description like '%cuberoll%'
		or @description like '%frankfurter%' or @description like '%golfera%' or @description like 'meat balls%' or @description like 'alb %' or @description = 'bragoli'
		or @description like '%luncheomeat%' or @description like 'it-tokk %'  or @description like 'themeatco%' or @description like '%mince%' or @description like 'meat ball%'
		or @description like '%cubetti%')
			begin
				update pre_process_transactions
				set description = 'MEAT PRODUCTS'
				where id = @id
			end

		if(@description like '%MAYOR%' or @description like '%KETCHUP%' or @description like '%calypso%' or @description like '%heinz%'
		or @description like '%tabasco%' or @description like '%colmans%' or @description like '%hp%' or @description like '%3hills%' or @description like '%hmns%'
		or @description like '%amoy%' or @description like '%arluy%' or @description like '%balsamico%' or @description like '%soy s%' or @description like '%dressing%' or @description like '%vinaigrette%'
		or @description like '%worchester%' or @description like '%sour cream%' or @description like '%mustard%' or @description like '%helmann%' or @description like 'pinto %' or @description like 'vol %'
		or @description like '%swt&sour%' or @description like 'pinto%' or @description like 'hellmann%' or @description like 'tt %' or @description like 'tzatziki %')
			begin
				update pre_process_transactions
				set description = 'CONDAMENTS'
				where id = @id
			end

		if(@description like '%LAMB%' or @description like '%uncle ben%' or @description like '%cinnamon%' or @description like '%bovril%' or @description like '%salt%' or @description like '%pepper%'
		or @description like 'baking powder%' or @description like '%seasoning%' or @description like 'bisto %' or @description like '% vinegar %' or @description like 'nutmeg %' or @description like 'whiting%'
		or @description like 'vegetable cubes%'  or @description = 'SUGAR 10*1-KG' or @description like 'sugar paste%' or @description like '% yeast %' or @description like 'mini sal fine%' or @description = 'caster sugar'
		or @description like 'demerrara sugar%' or @description like 'bebocooking%' or @description like 'castor sugar%' or @description = 'lard')
			begin
				update pre_process_transactions
				set description = 'COOKING INGREDIENTS'
				where id = @id
			end

		if(@description like '%water%' or @description like '%perrier%' or @description like '%aqua%' or @description like '%fontana%' or @description like '%smichel%' or @description like '%frizzante%'
		or @description like '%kristal%' or @description like 'rochetta %' or @description like 'rocchetta %' or @description like 'blue keld%' or @description like 'family pack %')
			begin
				update pre_process_transactions
				set description = 'WATER'
				where id = @id
			end

		if(@description like '%bolero%' or @description like '%coolee%' or @description like '%just%' or @description like 'milo%' or @description like 'nesquik%' or @description like 'ovaltine%'  or @description like 'sunquick %'
		or @description like '%solero%' or @description like 'S/O%powder%')
			begin
				update pre_process_transactions
				set description = 'WATER SOLUBLES'
				where id = @id
			end

		if(@description like '%bonduelle%' or @description like '%elite%' or @description like '%rio%' or @description like '%polpa%' or @description like '%kunserva%' or @description like '%three%'
		or @description like '%hills%' or @description like '%skipjack%' or @description like '%olives%' or @description like '%sundried%' or @description like '%capers%' or @description like '%kappar%'
		or @description like 'j.best%' or @description like 'la dorita%' or @description like 'lombardi%' or @description like 'marin %' or @description like 'princes %' or @description like 'sacla %'
		or @description like 'baked beans%' or @description like 'beans%' or @description like 'bordon%' or @description like 'camel brand%' or @description like 'la doria %' 
		or @description like 'mussels %' or @description like 'passata %' or @description like 'pesto %' or @description like 'puttanesca %'or @description like 'bax seafood%' or @description like 'black treacle %'
		or @description like 'zwan %' or @description like 'vera %'  or @description like 'tiko paella %' or @description like 'seafood cocktail%' or @description like 'royal star%' or @description like 'fruit cocktail %'
		or @description like '%servettes%' or @description like 'sharwoods%' or @description like 'seabreeze%' or @description = 'pesto' or @description like 'octopus in marina%' or @description like 'groot chick%'
		or @description like 'cannelini beans%' or @description like 'dany octopus%')
			begin
				update pre_process_transactions
				set description = 'PRESERVATIVES'
				where id = @id
			end

		if(@description like '%FCLARKS%' or @description like 'eviva %' or @description like 'kyg %' or @description like 'marmite %' or @description like '%jam%' or @description like '%remia%' or @description like '%spread%'
			or @description like '%marmalade%')
			begin
				update pre_process_transactions
				set description = 'SPREADS'
				where id = @id
			end

		if(@description like '%coleslaw%' or @description like '%zebbug%' or @description like '%bruschetta%' or @description like '%potato salad%' or @description like 'fazola %'
			or @description like 'gardiniera%' or @description like 'harissa %' or @description like 'fazola%' or @description like 'caponata%' or @description like 'giardiniera%')
			begin
				update pre_process_transactions
				set description = 'SIDES'
				where id = @id
			end
		
		if(@description like '%belvita%' or @description like '%benecol%' or @description like '%branettes%' or @description like '%danone%' or @description like '%diet%'
		or @description like '%eat natural%' or @description like '%fit food%' or @description like '%fitness%' or @description like '%flora%' or @description like '%florette%'
		or @description like '%smoochie%' or @description like '%smoothie%' or @description like '%snack-a%' or @description like '%galletti%' or @description like '%tipiak%' 
		or @description like '%cous%cous%' or @description like '%trays%' or @description like '%cerelac%' or @description like '%egg%' or @description like '%protein%' or @description like '%gymline%'
		or @description like '%healthline%' or @description like '%vitabakes%' or @description like '%flapjack%' or @description like '%innocent%' or @description like '%asiago%' 
		or @description like '%ba 5%' or @description like '%ba cberry%' or @description like '%salad%' or @description like '%er puffs%' or @description like '%ga nutty%' or @description like '%aloe vera%'
		or @description like '%zahar%' or @description like 'misura%' or @description like 'quaker %' or @description like 'quinoa%' or @description like 'ge %' or @description like 'organic %'
		or @description like 'wholemeal%' or @description like 'vitalite light%' or @description like 'vita natura %' or @description like 'ryvita%' or @description like '3 eviva %' or @description like 'nutal fiocco%'
		or @description like '%honey%' or @description like 'greek vine %' or @description like '%vegeterian%' or @description like '%vegetarian%' or @description like 'jordans %' or @description like 'krupuk %'
		or @description like 'lir %' or @description like 'm.nat%' or @description like 'nutramino %' or @description like '%oats%' or @description like 'pascual %' or @description like 'powerbar%'
		or @description like 'treats%' or @description like 'trust crunch %' or @description like 'schnekoppe %')
			begin
				update pre_process_transactions
				set description = 'HEALTHY PRODUCTS'
				where id = @id
			end

		if(@description like '%DEKORA%' or @description like '%trendi%' or @description like '%matches%' or @description like '%garbage%' or @description like '%aftersun%' or @description like '%lotion%'
		or @description like '%lighter%' or @description like '%drum%' or @description like '%rizla%' or @description like '%cig%' or @description like '%rolling%' or @description like '%ice cube%'
		or @description like '%ice flake%' or @description like '%cup%' or @description like '%charcoal%' or @description like '%disposable%' or @description like '%woven%' or @description like '%babylino%'
		or @description like '%bicarbonate%' or @description like '%deposit%' or @description like '%empty%' or @description like '%e-liquid%' or @description like '%enr alk%' 
		or @description like '%detergent%' or @description like '%deet%' or @description like '%gillette%' or @description like '%greeting%' or @description like 'lesieur%' or @description like 'lunch bags%'
		or @description like 'lynx%' or @description like 'mul bianco%' or @description like 'ocean spray%' or @description like 'pampers%' or @description like 'plasmon%' or @description like 'ryner%'
		or @description like 'whiskas%' or @description like '%hankies%' or @description like 'always%' or @description = 'AM' or @description like 'aptam%' or @description like 'beauty %' 
		or @description like 'bruncell %' or @description like 'cif %' or @description like '%wash%' or @description like 'delivery %' or @description like 'servettes %' or @description like '%straw%' 
		or @description like 'italia %' or @description like 'jb %' or @description like 'kuhne %' or @description like 'kuj %' or @description like 'marlboro %' or @description like 'ml bianco %' 
		or @description like 'obc %' or @description like 'old holborn %' or @description like 'olly%' or @description like 'oral b %' or @description like '%bags%' or @description like 'party menu%'
		or @description like 'per condire %' or @description like 'persil %' or @description like 'picture print %' or @description like 'pueblo %' or @description like '%qamar%' or @description like '%rayner%'
		or @description like 'ba cbery %'or @description like 'bayerland %' or @description like 'bouch?e %' or @description like 'bruncell %' or @description = 'bukkett qronfol' or @description like 'bulgari %'
		or @description like '%skewer%' or @description like 'windel %' or @description like 'vidal rolla %' 
		or @description like 'vandenbulcke%' or @description like '% beanie %' or @description like 'truvia %' or @description like 'treffin %' or @description = 'traditional mini flaky maltese' or @description like 'tonon %'
		or @description like 'tobacco %' or @description like '%napkins%' or @description like 'strained yog%' or @description like '% handkerchief% ' or @description like 'sma pro%' or @description like 'serie lei%'
		or @description like 's.t.%' or @description like 's&l %' or @description like 'roma %' or @description like 'pantene %' or @description like 'ocb %' or @description like 'ml.bianco %' or @description like '% funny money %' 
		or @description like '8254 %' or @description like 'amica %' or @description = 'bruncell' or @description = 'danube waves' or @description like '%tobacco%' or @description like 'dig avena%' or @description like 'dlle 690%'
		or @description like 'dulciar%' or @description like 'frit %' or @description = 'frolla' or @description = 'weighted item.' or @description like 'vanillin%' or @description like 'zip%' or @description like '%tabacco%'
		or @description like 'vival cranberry%' or @description like 'vandebuckle%' or @description like 'tuna nicoise %' or @description like 't4698%' or @description like '%handkerchiefs%' or @description like 'scarlino%' 
		or @description like 'savoiardi%' or @description like 'santero%' or @description like 'sajf%' or @description like 's.t. hyper%' or @description = 'roma' or @description like 'pouch %' or @description like 'plaice%'
		or @description like 'pj masks%' or @description like 'per condire%' or @description like 'pedigree%' or @description like 'pears williams%' or @description like 'parmalat %' or @description like 'paper funny%' or @description like 'pally marie%'
		or @description like 'mozart triangles%' or @description like 'mastro lindo%' or @description like 'marjoram%' or @description like '%mackarel ala weekend%' or @description = 'longs' or @description like 'lisbon %' or @description = 'ligeras' or @description like 'lotus%'
		or @description = 'krops' or @description like '%b/lino%' or @description like '%valencia orange%' or @description like 'e-go%' or @description like 'elan%' or @description like 'gizeh silver%' or @description like 'glb bel%' or @description like 'glenstal %'
		or @description like 'h&s %' or @description like '%cafe creme %' or @description like 'haddock %' or @description like 'hulula %' or @description like 'infacare %' or @description like 'inter %' or @description like 's.t hyper %' or @description like 'mr.day %'
		or @description like 'mil van%' or @description like 'manner%' or @description like 'lazz dol%' or @description like 'lacasitos%' or @description like 'kenco%' or @description like 'lar stock%' or @description like 'hulala %' or @description like 'ipsa sania%'
		or @description like 'kar stock%' or @description like 'baking cases %' or @description like '%ants killer' or @description like 'book%' or @description = 'davidoff' or @description = 'detergents' or @description like 'discount%' or @description like 'dove%'
		or @description like 'ad praline%' or @description like '%casereccio%' or @description like '1OFF%' or @description like 'anna faggio%' or @description like '%pink lady%' or @description like 'duracell%' or @description like '%plasters%'
		or @description like 'foxs%' or @description = 'guze' or @description like 'gulepp%' or @description like 'guze%' or @description like '%kontorn%' or @description like '%body spray%' or @description like '%lunch bag%' or @description like 'mail%'
		or @description like '%print%' or @description like 'mega duo%' or @description like 'mulin %' or @description like 'epic neptu %' or @description like 'farleys %' or @description like 'milan %'or @description like 'odyssey %'
		or @description like 'ritagli %' or @description like '%toothbrush%' or @description like '%tissues%' or @description like '% drages %' or @description like '% libretti%' or @description = 'sponge' or @description like 'swiss luxury %' or @description like 'tampax %'
		or @description like 'thermos %' or @description like 'valletta %' or @description like 'vanderbulcke%' or @description like 'wenswax%' or @description like 'fiorillo%' or @description like '%frylight%' or @description like 'villa guelfa %' or @description like 'nivea %'
		or @description = 'odyssey' or @description like 'p.s dry %' or @description like 'pif-paf %' or @description like '%lenor%' or @description like '%ariel%' or @description like '%fairy%' or @description like '%snaps plain%' or @description like 'bloomer %' 
		or @description like '%bonomi%' or @description like 'assortment %' or @description like 'cards %' or @description like '%stollen%' or @description like 'daily mirror %' or @description like 'deluxe %' or @description like 'gas cartridge%' or @description like 'isobella %'
		or @description = 'nazzjon' or @description like 'tate&layle %' or @description like 'tate&lyle %' or @description like 'trebormightie%' or @description like 'sacchi %' or @description like '% sens small %' or @description like '% cling film %' or @description like 'per tavoletta %'
		or @description like 'party barquettes %' or @description = 'sunday times' or @description = 'times')
			begin
				update pre_process_transactions
				set description = 'OTHERS'
				where id = @id
			end

		if(@description like '%carrot%' or @description like '%onion%' or @description like '%cabbage%' or @description like '%lettuce%' or @description like '%rucola%'
		or @description like '%cauliflower%' or @description like '%chives%' or @description like '%corn%' or @description like '%cucumber%' or @description like '%date%' or @description like '%broccoli%'
		or @description like '%broad%' or @description like '%mushrooms%' or @description like '%garlic%' or @description like '%insalata%' or @description like '%kiwi%' or @description like '%asparagus%'
		or @description like '%spinach%' or @description like '%barley%' or @description like '%basil%' or @description like '%bay leaves%' or @description like '%colombus%' 
		or @description like '%fennel%' or @description like '%fruit and veg%' or @description like '%gherkins%' or @description like '%greenpeas%' or @description like 'lisbon banana%' 
		or @description like 'mushroom%' or @description like 'oranges%' or @description like 'potatoes%' or @description like 'apple%' or @description like 'artichoke%' or @description like 'baxters %'
		or @description like 'cloves %' or @description like 'life %' or @description like 'pruna %' or @description = 'mint' or @description = 'mixed herbs' or @description like 'oregano %'
		or @description like 'parsley %' or @description like 'pumpkin %' or @description like 'anise %' or @description = 'bp dried mix fruits%' or @description like 'valfrutta %' or @description like '%cloves ground'
		or @description = 'thyme'  or @description like 'tomatoes %' or @description = 'shallots' or @description like 'sesame %' or @description like 'saffron%' or @description like 'rocket leaf%' or @description = 'parsley'
		or @description = 'parsnip ' or @description = 'oregano' or @description like 'bp dried %' or @description like 'chickpeas %' or @description like '% fruttas %' or @description like 'royal gala%' or @description like '%seed%'
		or @description like '%artichokes%' or @description like '%pruna%' or @description like 'chickepeas%' or @description like 'koko %' or @description like 'koka %' or @description = 'FRUIT & VEGETABLE'
		or @description like 'banana%' or @description like 'beetroot%' or @description = 'blackberries' or @description = 'blueberries' or @description = 'brussel sprouts' or @description like 'celery%' or @description = 'coconut'
		or @description like 'cranberries%' or @description like 'bayleafs%' or @description like 'black currants%' or @description like '%garni%' or @description like '%mushs%' or @description like '%mint%' or @description like '%parsley%'
		or @description like 'fruit mix %' or @description = 'leeks' or @description like 'lemons%' or @description like '%wildberries%' or @description like 'mandarins%' or @description like 'mandul %' or @description = 'mangetout'
		or @description = 'mango' or @description = 'marrows' or @description like 'marrows %' or @description like 'melons%' or @description like 'mil %' or @description like '%mint%' or @description like '%cherries%' or @description like '%fruit%' or @description like '%peels%'
		or @description = 'nectarine' or @description like 'peaches %' or @description like 'pears %' or @description = 'plums' or @description like '%raisins%' or @description = 'raspberries'
		or @description like '%raspberry%' or @description like 'm/rooms%' or @description like 'pear %' or @description like '%beans%' or @description like '%peas%' or @description like 'sultana%' or @description like 'thyme%' or @description like '%cherry tomatoes'
		or @description like 'whole %' or @description like 'yofu%' or @description like 'pinapple %' or @description like 'pineapple%' or @description like 'plums %')
			begin
				update pre_process_transactions
				set description = 'FRUIT AND VEGETABLES'
				where id = @id
			end

		if(@description like '%chilly%' or @description like '%spice%' or @description like '%coriander%' or @description like '%cumin%'
		or @description like '%curry%' or @description like '%sage%' or @description like '%spices%' or @description like '%turmeric%' or @description like '%tarragon%'
		or @description like '%chilli%' or @description like '%paprika%' or @description = 'garam masala' or @description like '%figs%' or @description like 'ground %' or @description like '%herbs%' or @description like 'tandoori %' or @description like 'turmerric %')
			begin
				update pre_process_transactions
				set description = 'SPICES'
				where id = @id
			end

		if(@description like '%cisk%' or @description like '%skol%' or @description like '%heineken%' or @description like '%vodka%' or @description like '%wine%'
		or @description like '%skol%'or @description like '%whisky%' or @description like '%red%' or @description like '%rose%' or @description like '%merlot%' 
		or @description like '%chardonnay%' or @description like '%girgentina%' or @description like '%blue label%' or @description like '%carlsberg%' 
		or @description like '%corona%' or @description like '%corvo%' or @description like '%bavaria%' or @description like '%blue nun%' or @description like '%martini%'
		or @description like '%JACK DANIEL%' or @description like '%green label%' or @description like '%sauvignon%' or @description like '%moscato%' or @description like '%gavi%'
		or @description like '%la torre%' or @description like '%beer%' or @description like '%verdala%'or @description like '%shiraz%' or @description like '%bells%'
		or @description like '%chenin%' or @description like '%jagermeister%' or @description like '%chivas%' or @description like '%hopleaf%' or @description like '%woodpecker%'
		or @description like '%malibu%' or @description like '%smirnoff%' or @description like '%la%valette%' or @description like '%aperol%' or @description like '%bacardi%'
		or @description like '%baileys%' or @description like '%becks%' or @description like '%bosca%' or @description like '%bottega%' or @description like '%brioblu%' or @description like '%budweiser%'
		or @description like '%bulmers%' or @description like '%disaronno%' or @description like '%dolcino%' or @description like '%shandy%' or @description like '%comfort%' or @description like '%gin%'
		or @description like '%rosato%' or @description like '%cider%' or @description like '%vino%' or @description like '%captain morgan%' or @description like '%caravaggio%' 
		or @description like '%citadella%' or @description like '%cabarnet%' or @description like '%sangiovese%' or @description like '%aperitivo%' or @description like '%syrah%' or @description like '%cuvee%'
		or @description like '%dard%' or @description like '%frangelico%' or @description like '%hennessy%' or @description like '%guiness%' or @description like '%whiskey%' or @description like '%johnnie%' 
		or @description like '%chenet%' or @description like '%COINTREAU%' or @description like '%croft%' or @description like '%ct rv%' or @description like '%dewars%' or @description like '%freschello%'
		or @description like '%gato negro%' or @description like '%guinness%' or @description like 'jcreek%' or @description like 'jwk%' or @description like 'lachryma%' or @description like 'landini%' 
		or @description like 'le uve%' or @description like 'medina %' or @description like 'moet %' or @description like 'molinari%' or @description like '%limoncello%' or @description like 'talisker%'
		or @description like 'tall horse %' or @description like 'tavernello%' or @description like 'tequila%' or @description like 'averna%' or @description like 'badgers%' or @description like 'belvedere %'
		or @description like 'bertolli%' or @description like 'campari %' or @description like 'canti %' or @description like 'cynar %' or @description like '% curacao %' or @description like '%sauv%' 
		or @description like '%pinot grigio %' or @description like '%pinotage%' or @description like 'offley %' or @description like 'passoa %' or @description like '%35 south%' or @description like 'zeppi%'
		or @description like 'villa albius%' or @description like 'torres %' or @description like 'santa helena%' or @description like 'sangria %' or @description like 'S R%' or @description like '% chianti %'
		or @description like '770 %' or @description like 'fiesta %' or @description like 'charles du luc%' or @description like 'antonin%' or @description like '%anisette%' or @description like '%sancerre%'
		or @description like '%amara%' or @description like 'filipetti %' or @description like 'glenmorangie%' or @description like 'hxm%' or @description like 'kahlua %' or @description like 'ruffino%' or @description like 'san valentino %'
		or @description like 'sottovoce %' or @description like 'cassar de %' or @description like 'cheval franc %' or @description like 'farsons %' or @description like 'frontera %' or @description like 'leila amaretto %'
		or @description like 'mertes %' or @description like 'stowells %')
			begin
				update pre_process_transactions
				set description = 'ALCOHOL'
				where id = @id
			end

	fetch next from line_cursor into
		@description,
		@id
	end

close line_cursor
deallocate line_cursor

insert into categorised_transactions
	(transaction_number, [description], item_number, quantity, unit_price)
select	transaction_number,
		[description],
		item_number,
		quantity,
		unit_price
from pre_process_transactions

--further cleaninig of data where there is only 1 item in the transaction
--after categorisation of data
declare @Cursor cursor;
declare @Field int;
BEGIN
    SET @Cursor = cursor for
    select number from transactionheaders      

    open @Cursor 
    fetch next from @Cursor 
    into @Field

    while @@FETCH_STATUS = 0
    begin
		if(1 = (select count(item_number) 
			from categorised_transactions
			where transaction_number = @Field))
		begin
			delete from transactionheaders
			where number = @Field

			delete from categorised_transactions
			where transaction_number = @Field
		end
      fetch next from @Cursor 
      into @Field 
    end; 

    close @Cursor ;
    deallocate @Cursor;
end;

select * from categorised_transactions

select distinct [description] from categorised_transactions

delete from CombinedTransactionDescriptions

insert into CombinedTransactionDescriptions
	(transactionNumber, transactionItems)
SELECT th.number, 
concat('[',left(+SUBSTRING(
    (
		SELECT distinct tl.[description]+','
        FROM dbo.categorised_transactions tl
        WHERE tl.transaction_number = th.number
        FOR XML PATH ('')
    ), 1, 10000) , len(SUBSTRING(
    (
		SELECT distinct tl.[description]+','
        FROM dbo.categorised_transactions tl
        WHERE tl.transaction_number = th.number
        FOR XML PATH ('')
    ), 1, 10000))-1),']')
FROM transactionheaders th

select count(*) from transactionheaders
select count(*) from CombinedTransactionDescriptions

select count(*) as 'total' from categorised_transactions
select count(*) as 'bread' from categorised_transactions where [description] = 'bread'
select count(*) as 'cakes and sweets' from categorised_transactions where [description] = 'cakes and sweets' 
select count(*) as 'milk and dairy products' from categorised_transactions where [description] = 'milk and dairy products'

select count(*) as 'total' from CombinedTransactionDescriptions
select count(*) as 'bread' from CombinedTransactionDescriptions where transactionItems like '%bread%' 
select count(*) as 'cakes and sweets' from CombinedTransactionDescriptions where transactionItems like '%cakes and sweets%'
select count(*) as 'milk and dairy products' from CombinedTransactionDescriptions where transactionItems like '%milk and dairy products%'