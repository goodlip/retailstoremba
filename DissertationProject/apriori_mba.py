import pandas as pd
import pyodbc as db
from mlxtend.frequent_patterns import apriori, association_rules
from mlxtend.preprocessing import TransactionEncoder

# from efficient_apriori import apriori as ef_ap


database = 'BugibbaTransactions'

conn = db.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                  SERVER=DESKTOP-SPUSCSS; \
                  DATABASE=' + database + ';\
                  Trusted_Connection=yes;')

# combined = "SELECT * FROM CombinedTransactionDescriptions_Test"
combined = "SELECT * FROM CombinedTransactionDescriptions"

df_combined = pd.read_sql(combined, conn)

all_transaction_items = df_combined['transactionItems'].str.cat(sep=',')

array = all_transaction_items.split('],[')
all_transaction_items_df = pd.DataFrame(data=array)

all_transaction_items_df[0][0] = all_transaction_items_df[0][0].replace('[', '')

transaction_list = []

for item in all_transaction_items_df[0]:
    item_list = item.split(',')
    transaction_list.append(item_list)

# NORMAL APRIORI
te = TransactionEncoder()
te_ary = te.fit(transaction_list).transform(transaction_list)
df = pd.DataFrame(te_ary, columns=te.columns_)

frq_items = apriori(df, min_support=0.2, use_colnames=True)
print(frq_items.head)
rules = association_rules(frq_items, metric="lift", min_threshold=1)
print(rules.head)

# SPARSE APRIORI
oht_ary = te.fit(transaction_list).transform(transaction_list, sparse=True)
sparse_df = pd.DataFrame.sparse.from_spmatrix(oht_ary, columns=te.columns_)
sparse_df

frq_items_sparse = apriori(sparse_df, min_support=0.2, use_colnames=True)
rules_sparse = association_rules(frq_items_sparse, metric="lift", min_threshold=1)
rules_sparse = rules_sparse.sort_values(['confidence', 'lift'], ascending=[False, False])
print(rules_sparse.head)

# EFFICIENT APRIORI
#itemsets, rules = ef_ap(transaction_list, min_support=0.2, min_confidence=0.5)
#print(rules)
