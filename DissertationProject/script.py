import pandas as pd
import numpy as np
import pyodbc as db

transactionNumber = 0
for count in range(1, 17):
    df = pd.read_csv("C:\\Users\\seang\\Desktop\\School\\Degree\\Third Year\\Dissertation\\Bugibba\\BugibbaTransaction"
                     + str(count) + ".csv", header=None, encoding='unicode_escape')
    dfHeaders = pd.DataFrame()
    dfLines = pd.DataFrame()

    dfLines['transNumber'] = np.nan

    for index, row in df.iterrows():
        if (row[0] != 'MAYPOLE FRANCHI'):
            dfLines = dfLines.append(df.iloc[index])
            dfLines['transNumber'][dfLines.index[-1]] = transactionNumber
        else:
            transactionNumber = row[2]
            dfHeaders = dfHeaders.append(df.iloc[index])

    print("Done")

    dfLines.head()

    dfLines = dfLines.drop(dfLines.columns[[1, 2, 3, 4, 6, 7, 11, 12]], axis=1)
    dfHeaders = dfHeaders.drop(dfHeaders.columns[[1, 6, 7, 8, 9, 10]], axis=1)

    dfLines.columns = ['Transaction Number', 'Item Number', 'Item Description', 'Quantity Sold', 'Unit Price']
    dfHeaders.columns = ['Company Name', 'Transaction Number', 'Date', 'Time', 'Transaction Type', 'Transaction Total']

    # DROP WHERE QUANTITY IS 0 AND ITEM NUMBER IS NaN
    dfLines.drop(dfLines[pd.isna(dfLines['Item Number'])].index, inplace=True)
    dfLines.drop(dfLines[dfLines['Quantity Sold'] == 0].index, inplace=True)
    dfHeaders.drop(dfHeaders[dfHeaders['Transaction Total'] <= 0].index, inplace=True)

    server = 'DESKTOP-SPUSCSS'
    database = 'BugibbaTransactions'

    conn = db.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                      SERVER=' + server + '; \
                      DATABASE=' + database + ';\
                      Trusted_Connection=yes;')

    cursor = conn.cursor()

    insert_headers = '''INSERT INTO transactionheaders (company_name, number, date, time, type, total)
                      VALUES (?, ?, CONVERT(DATETIME, ?, 103), convert(varchar(32), ?, 8), ?, ?);'''

    for i, row in dfHeaders.iterrows():
        print(i)
        values = (row[0], row[1], row[2], row[3], row[4], row[5])

        cursor.execute(insert_headers, values)

    insert_lines = '''INSERT INTO transactionlines (transaction_number, description, item_number, quantity, unit_price)
                      VALUES (?, ?, ?, ?, ?);'''

    for i, row in dfLines.iterrows():
        print(i)
        values = (row[0], row[2], row[1], row[3], row[4])

        cursor.execute(insert_lines, values)

    conn.commit()

    cursor.close()
    conn.close()
