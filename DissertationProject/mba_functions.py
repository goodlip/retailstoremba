import pandas as pd
import pyodbc as db
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType, IntegerType, StructType, StructField
from pyspark.ml.fpm import FPGrowth
from pyspark.sql.functions import col, split
from mlxtend.frequent_patterns import apriori, association_rules
from mlxtend.preprocessing import TransactionEncoder


def get_data_from_database(store_location):
    database = store_location + 'Transactions'

    conn = db.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                      SERVER=DESKTOP-SPUSCSS; \
                      DATABASE=' + database + ';\
                      Trusted_Connection=yes;')

    combined = "SELECT * FROM CombinedTransactionDescriptions"
    # combined = "SELECT * FROM CombinedTransactionDescriptions_Test"
    df_combined = pd.read_sql(combined, conn)

    return df_combined


def format_data_for_mining(df_combined):
    all_transaction_items = df_combined['transactionItems'].str.cat(sep=',')

    array = all_transaction_items.split('],[')
    all_transaction_items_df = pd.DataFrame(data=array)

    all_transaction_items_df[0][0] = all_transaction_items_df[0][0].replace('[', '')

    all_transaction_items_df.insert(0, 'id', range(1, 1 + len(all_transaction_items_df)))
    all_transaction_items_df.columns = ['id', 'items']

    return all_transaction_items_df


def perform_mba(all_transaction_items_df, min_support, min_confidence):
    spark = SparkSession.builder.getOrCreate()

    my_schema = StructType([StructField("id", IntegerType(), True) \
                               , StructField("items", StringType(), True)])

    df = spark.createDataFrame(all_transaction_items_df, my_schema)
    df = df.withColumn('items', split(col('items'), ','))

    fp_growth = FPGrowth(itemsCol='items', minSupport=min_support, minConfidence=min_confidence)

    model = fp_growth.fit(df)

    return model


def get_transaction_lines(store_location):
    database = store_location + 'Transactions'

    conn = db.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                      SERVER=DESKTOP-SPUSCSS; \
                      DATABASE=' + database + ';\
                      Trusted_Connection=yes;')

    combined = "SELECT * FROM transactionlines"

    df_lines = pd.read_sql(combined, conn)

    return df_lines


def main(store_location, min_support, min_confidence):
    df_combined = get_data_from_database(store_location)
    all_transaction_items_df = format_data_for_mining(df_combined)

    model = perform_mba(all_transaction_items_df, min_support, min_confidence)

    model.freqItemsets.show()
    model.associationRules.show()


main("Bugibba", 0.2, 0.2)
main("Iklin", 0.2, 0.2)
main("Fgura", 0.2, 0.2)
