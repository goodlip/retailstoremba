import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from apyori import apriori
import pyodbc as db


def get_data_from_database(store_location):
    database = store_location + 'Transactions'

    conn = db.connect('DRIVER={ODBC Driver 17 for SQL Server}; \
                      SERVER=DESKTOP-SPUSCSS; \
                      DATABASE=' + database + ';\
                      Trusted_Connection=yes;')

    #combined = "SELECT * FROM CombinedTransactions"
    combined = "SELECT * FROM CombinedTransactionDescriptions"

    df_combined = pd.read_sql(combined, conn)

    return df_combined


def format_data_for_mining(df_combined):
    all_transaction_items = df_combined['transactionItems'].str.cat(sep=',')

    array = all_transaction_items.split('],[')
    all_transaction_items_df = pd.DataFrame(data=array)

    all_transaction_items_df[0][0] = all_transaction_items_df[0][0].replace('[', '')

    all_transaction_items_df.insert(0, 'id', range(1, 1 + len(all_transaction_items_df)))
    all_transaction_items_df.columns = ['id', 'items']

    return all_transaction_items_df

def perform_mba(all_transaction_items_df, min_support, min_confidence):
    association_rules = apriori(all_transaction_items_df, min_support=min_support, min_confidence=min_confidence, min_lift = 3, min_length=2)
    association_results = list(association_rules)
    
    return association_results

def main(store_location):
    df_combined = get_data_from_database(store_location)
    all_transaction_items_df = format_data_for_mining(df_combined)

    model = perform_mba(all_transaction_items_df, 0.02, 0.2)

    print(len(model))
    
main("Fgura")

